import numpy as np
import tensorflow as tf
from keras.layers import GRU, Dropout, Bidirectional, Embedding, Dense, TimeDistributed, Flatten, Input
from keras.activations import tanh
from keras.activations import tanh,softmax
from tqdm import tqdm

from attention import BahdanauAttention
from encoder import Encoder
from decoder import Decoder

class s2s_translator  :
    def __init__(self, input_words, epochs, nb_batch, voc_size, embedding_size, hidden_shape, dropout):
        super().__init__()
        self.optimizer = tf.optimizers.Adam()
        self.loss_fn = tf.compat.v1.losses.sparse_softmax_cross_entropy
        self.epochs = epochs
        self.nb_batch = nb_batch
        
        # Modèle :
        encoder_input = Input(1)
        encode = Encoder(voc_size, embedding_size, hidden_shape, dropout)(encoder_input)
        self.encoder = tf.keras.Model(inputs= encoder_input, outputs= encode)

        decoder_input = Input(1) # input_decoder : (batch_size, 1)
        enc_state_input = Input((input_words, hidden_shape * 2)) # enc_output : (batch_size, 30, 1024)
        state_input = Input(hidden_shape) # state : (batch_size, 512)
        decode = Decoder(voc_size, embedding_size, hidden_shape, dropout)((decoder_input, enc_state_input, state_input))
        self.decoder = tf.keras.Model(inputs = [decoder_input, enc_state_input, state_input], outputs= decode)

    def train(self, query, target): # query = [None, len_sequences]; target = [None, len_sequences]
        
        optimizer, loss_fn = self.optimizer, self.loss_fn
        epochs, nb_batch = self.epochs, self.nb_batch

        total_example_intrain = np.shape(query)[0]
        batch_size = int(total_example_intrain/nb_batch)

        self.loss_history = []

        for epoch in range(epochs): #for each epoch

            batch_loss = tf.constant(0.0) #does not work with np.array([])

            for batch in tqdm(range(0, total_example_intrain, batch_size)): #for each batch
                
                with tf.GradientTape() as gradient :

                    loss_count = tf.constant(0.0)

                    query_batch = query[batch : batch + batch_size]
                    # print("batch_size :", batch_size)
                    # print("\nquery_batch :", np.shape(query_batch), '\n')
                    target_batch = target[batch : batch + batch_size]
                    
                    enc_output, state = self.encoder(query_batch)

                    for i in range(query_batch.shape[1]): # for each word

                        input_decoder = np.transpose([query_batch[:,i]]) # input(1)
                        # print("input_decoder :", np.shape(input_decoder))

                        output, state = self.decoder((input_decoder, enc_output, state))
                        # Calcul de la loss
                        loss_count = loss_count + loss_fn(target_batch[:, i], output)
                        # print("loss count :", loss_count)

                    grads = gradient.gradient(loss_count, self.encoder.trainable_weights + self.decoder.trainable_weights)
                    optimizer.apply_gradients(zip(grads, self.encoder.trainable_weights + self.decoder.trainable_weights))
                    batch_loss = batch_loss + loss_count

            self.loss_history.append(np.array(batch_loss))
        
        self.encoder.save("encoder_model")
        self.decoder.save("decoder_model")

    def translate_sentences (self, query, target): 
        
        values = []

        

        for sentences in range(0, np.shape(query)[0]): #for each sequences
            value = [] 
            enc_output, state = self.encoder(query)
            for i in range(query.shape[1]): #for each word

                input_decoder = np.transpose([query[:,i]]) # input(1)
                output, state = self.decoder((input_decoder, enc_output, state))
                value.append(np.argmax(np.array(output), axis = -1)[0])
            
            values.append(value)

        return values