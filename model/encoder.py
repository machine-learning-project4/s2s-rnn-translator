import numpy as np
import tensorflow as tf
from keras.layers import GRU, Dropout, Bidirectional, Embedding, Dense, TimeDistributed, Flatten
from keras.activations import tanh
from keras.activations import tanh,softmax

class Encoder (tf.keras.layers.Layer) :
    def __init__(self, voc_size, embedding_size, hidden_shape, dropout):
        super().__init__()
        self.embedding = Embedding(voc_size, embedding_size)
        self.gru = Bidirectional(GRU(hidden_shape, return_state=True, return_sequences=True))
        self.D = Dense(hidden_shape)
        self.dropout = Dropout(dropout)

    def call(self, input):

        # print("ENCODER DEBUG")

        embed = self.dropout(self.embedding(input))
        outputs, state_c, state_h = self.gru(embed)

        state = tf.concat([state_c, state_h], axis=1)
        state = self.D(tanh(state))

        # print("outputs shape :", np.shape(outputs))
        # print("state shape :", np.shape(state))
        # print("\n")
        
        # outputs shape : [batch size, len_sequences, hidden_shape * 2]
        # state shape : [batch_size, hidden_shape * 2]

        return outputs, state