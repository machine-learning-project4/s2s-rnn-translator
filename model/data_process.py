from keras.preprocessing.text import Tokenizer
import tensorflow as tf


class Tokenizer_seq :

    def __init__ (self, texts, max_tokens) :
        
        MAX_VOCAB_SIZE = 3000
        
        tokenizer = Tokenizer(num_words = MAX_VOCAB_SIZE, filters='')
        tokenizer.fit_on_texts(texts)
        self.index_to_word = dict(zip(tokenizer.word_index.values(), tokenizer.word_index.keys()))
        self.tokens = tokenizer.texts_to_sequences(texts)
        self.tokens = tf.keras.preprocessing.sequence.pad_sequences(self.tokens,
                                                                      maxlen= max_tokens,
                                                                      padding='post')
        self.voc_size = len(tokenizer.word_index) + 1

def token_to_word(tokens, tokenizer) :
    words = [tokenizer.index_to_word[token] for token in tokens if token != 0]
    # text = " ".join(words)
    return words