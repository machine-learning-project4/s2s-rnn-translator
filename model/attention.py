import numpy as np
import tensorflow as tf
from keras.layers import GRU, Dropout, Bidirectional, Embedding, Dense, TimeDistributed, Flatten
from keras import Input, Sequential, Model
from keras.activations import tanh
from keras.preprocessing.text import Tokenizer
from sklearn.model_selection import train_test_split
from keras.activations import tanh,softmax
from keras import backend as K  #to Transpose

class BahdanauAttention(tf.keras.layers.Layer) :
    def __init__(self, units):
        super(BahdanauAttention, self).__init__()
        self.W1 = Dense(units)
        self.W2 = Dense(units)
        self.V = Dense(1)

    def call(self, state, enc_states):

        # print("ATTENTION DEBUG")

        # hidden state dimensions adaptation to encoder states
        state = tf.expand_dims(state, axis = 1)
        state = tf.repeat(state, repeats = [np.shape(enc_states)[1]], axis = 1)
        score = self.V(tanh(self.W1(state) + self.W2(enc_states)))
        # print("score shape :", np.shape(score))
        attention = tf.squeeze(score, axis = 2)
        # print("attention shape :", np.shape(attention))
        context_vector = softmax(attention, axis = 1)
        
        # print("context_vector shape :", np.shape(context_vector))
        
        # print("\n\n")

        return context_vector