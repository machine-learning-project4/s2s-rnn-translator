import numpy as np
import tensorflow as tf
from keras.layers import GRU, Dropout, Bidirectional, Embedding, Dense, TimeDistributed, Flatten
from keras import Input, Sequential, Model
from keras.activations import tanh
from keras.preprocessing.text import Tokenizer
from sklearn.model_selection import train_test_split
from keras.activations import tanh,softmax
from keras import backend as K  #to Transpose

from attention import BahdanauAttention

class Decoder (tf.keras.layers.Layer):
    def __init__(self, voc_size, embedding_size, hidden_shape, dropout):
        super().__init__()
        self.gru = tf.keras.layers.GRU(hidden_shape, return_sequences=True, return_state=True)
        self.embedding = Embedding(voc_size, embedding_size)
        self.D = Dense(voc_size)
        self.attention = BahdanauAttention(units = hidden_shape)
        self.dropout = Dropout(dropout) 

    def call(self, inputs) :
        
        input, enc_states, state = inputs
        
        # print("DECODER DEBUG")
        input = K.permute_dimensions(input, (1, 0))
        # input = tf.expand_dims(input, axis = 0)
        # print("input :", np.shape(input)) # input : [1, batch_size]

        embed = self.dropout(self.embedding(input))
        # print("embed :", np.shape(embed)) # input : [1, batch_size, embedding_size]

        # Concaténation de embed, enc_states, context :
        context = self.attention(state,enc_states) 
        context = tf.expand_dims(context, axis = 1)
        # print("context :", np.shape(context)) # context = [batch_size, 1, len_sequences]
        
        # print("encoder shape :", np.shape(enc_states)) # enc_states = [batch_size, len_sequences, hidden_shape * 2]

        weight =  tf.linalg.matmul(context, enc_states)
        weight = K.permute_dimensions(weight, (1, 0, 2))
        # print("weight shape :", np.shape(weight)) # weight = [batch_size, 1, hidden_shape * 2]
        gru_input = tf.concat([embed, weight], axis = 2)
        gru_input = K.permute_dimensions(weight, (1, 0, 2))
        # print("gru_input :", np.shape(gru_input)) # gru_input = [batch_size, 1, (hidden_shape * 2) + embedding_size]

        # Couche RNN
        output, state = self.gru(gru_input, state) # gru_input = [batch, timesteps, feature]
        # print("state :", np.shape(state)) #state = [batch size, hidden_shape]
        
        # Prediction*
        output = K.permute_dimensions(output, (1, 0, 2))
        output = tf.squeeze(output, axis = 0)
        weight = tf.squeeze(weight, axis = 0)
        embed = tf.squeeze(embed, axis = 0)
        # print("embed :", np.shape(embed)) 
        # print("output :", np.shape(output)) #output = [batch size, hidden_shape] 
        # print("weight :", np.shape(weight))

        prediction = self.D(tf.concat((output, weight, embed), axis = 1)) # prediction = [batch size, voc_size]
        
        print("\n\n")
    
        return prediction, state